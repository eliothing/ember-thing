![](https://elioway.gitlab.io/eliothing/ember-thing/elio-ember-Thing-logo.png)

> EmberJs adons, **the elioWay**

# ember-thing ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

[EmberJs](emberjs.com) model, route, controller, component and template addons, built out of schema.org "Things", **the elioWay**.

- [ember-thing Documentation](https://elioway.gitlab.io/eliothing/ember-thing/)

## Installing

- [Installing ember-thing](https://elioway.gitlab.io/eliothing/ember-thing/installing.html)

## Requirements

- [eliothing Prerequisites](https://elioway.gitlab.io/eliothing/installing.html)

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Seeing is Believing

```
You're seeing it.
```

- [eliothing Quickstart](https://elioway.gitlab.io/eliothing/quickstart.html)
- [ember-thing Quickstart](https://elioway.gitlab.io/eliothing/ember-thing/quickstart.html)

# Credits

- [ember-thing Credits](https://elioway.gitlab.io/eliothing/ember-thing/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/eliothing/ember-thing/apple-touch-icon.png)
