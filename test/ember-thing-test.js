const should = require("chai").should()
const emberThing = require("../ember-thing")

describe("module | emberThing", function(hooks) {
  it("fetches emberThing", () => {
    emberThing().should.equal("ember-thing")
  })
})
